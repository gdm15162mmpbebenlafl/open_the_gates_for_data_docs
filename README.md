Mobigent   -- **Open_The_Gates_For_Data** 
=========

**Omschrijving van de opdracht:**
-------------
Maak een responsive mobile-first webapplicatie waarin minimaal 6 datasets, afkomstig uit de dataset-pool van Stad Gent, verwerkt zijn. Conceptueel denken is heel belangrijk in deze applicatie. Dit betekent dat jullie verbanden moeten leggen tussen deze datasets. Het is louter niet alleen het oplijsten en visualiseren van deze datasets, er moet een concept rond gebouwd worden.

Deze applicatie is bestemd voor +18 jarigen waarbij de gebruiksvriendelijkheid, interactiviteit, uitbreidbaarheid en schaalbaarheid van belang zijn.

----------


**Concept:**
MobiGent, Een mobile webapplicatie die de locaties toont van alle fietsverleningslocaties, parkeerplaatsen, DeLijn haltes en taxi-afhaalpunten in Gent.
Dit simpel en met een gebruiksvriendelijk ontwerp.

**MobiGent**
MobiGent is een webapplicatie die gebruik maakt van de datasets van Stad Gent.
Deze applicatie is geschikt voor mensen die snel willen zien waar er parkeer plaatsen zijn, of waar ze een tram/bus, fiets of taxi kunnen regelen. Dit verschijnt dan direct mooi zichtbaar op een kaartje, het kaartje toont enkel een plattegrond en markers duiden de locaties aan.

Ik heb gekozen voor een contrastrijk ontwerp, met gekleurde markers.
Dit zorgt voor een zeer simpel en duidelijk overzicht, bijvoorbeeld als men de map bezoekt van alle parkings in Gent, wordt je niet langer afgeleid door een hele hoop informatie.
Het contrastrijk design zorgt ervoor dat wat op moet vallen, direct opvalt.
De webapplicatie is mobile first en ziet er dus het best uit op een mobiel apparaat. Het is responsive en kan gerust ook op desktop worden gebruikt.

--------
**MobiGent site**

http://www.arteveldehogeschool.be/campusGDM/studenten_201516/ebenlafl/nmdad1/openthegatesfordata/

-----

**dossier**


http://www.arteveldehogeschool.be/campusGDM/studenten_201516/ebenlafl/nmdad1/openthegatesfordata/dossier.pdf